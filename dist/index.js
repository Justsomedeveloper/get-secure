/* Variable initalisation */
const lowercase = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
const uppercase  = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
const letters = uppercase.concat(lowercase);
const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9 , 0];
const specialCharacters = ["!", "\"", "#", "$", "%", "&", "'", "(", ")", "*", "+", ",", "-", ".", ";", ":", "[", "]"];

const generatePassword = document.getElementById('generatePassword');


/* Logic for password length */
const passwordLength = document.getElementById('passwordLength');
const passwordLengthValue = document.getElementById('passwordLengthValue');
passwordLengthValue.innerHTML = passwordLength.value;

passwordLength.oninput = function() {
    passwordLengthValue.innerHTML = this.value;
}


/* Functions to generate the final password */
const createPassword = () => {
    let selectedArray = [];

    const specialCharacterToggleValue = document.querySelector('#specialCharacterToggle').checked;
    selectedArray = specialCharacterToggleValue ? letters.concat(numbers, specialCharacters) : letters.concat(numbers);
    return passwordGeneration(selectedArray);
}

const passwordGeneration = (selectedArray) => {
    let finalPassword = '';
    for(let i =0; i < passwordLength.value; i++) {
        let character = selectedArray[Math.floor(Math.random() * selectedArray.length)];
        finalPassword += character;
 
    }
    return finalPassword
}

generatePassword.onclick = function() {
    passwordField.innerHTML = createPassword();
    passwordField2.innerHTML = createPassword();
}